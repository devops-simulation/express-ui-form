var express = require('express');
var router = express.Router();
const multer  = require('multer')
const path = require('path');
const config = require("../config/config");
const environment = process.env[`SERVER_ENV`] || 'local';

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'tmp/');
    },

    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

const upload = multer({storage: storage})
let axios = require('axios');
let fs = require('fs');
const FormData = require('form-data');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' , result: 0});
});

router.post('/',upload.single('fname'), async function(req, res, next) {

  const apiUrl = config[environment].apiUrl;
  let filePath = req.file.path
  const form = new FormData();
  const fileStream = fs.createReadStream(filePath);
  form.append('file', fileStream);

  const options = {
      method: 'POST',
      url: apiUrl,
      headers: {
        'Content-Type': 'multipart/form-data; boundary=---011000010111000001101001'
      },
      data: form
  };

  try{
    let result = await axios.request(options);
    res.render('index', { title: 'Express' ,result: result.data});
  }catch(e){
    console.log(e.msg)
    res.render('index', { title: 'Express' ,result: {'average':NaN}});
  }

});

module.exports = router;
