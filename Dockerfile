FROM node:16-alpine

RUN mkdir /opt/apps -p

ADD . /opt/apps

WORKDIR /opt/apps

RUN npm install && npm cache clean -f

CMD ["npm","run","local"]